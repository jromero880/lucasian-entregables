-- Create tables section -------------------------------------------------

-- Table cliente

CREATE TABLE "cliente"
(
  "cif_cliente" Character varying(15) NOT NULL,
  "nombre" Character varying(60),
  "apellido1" Character varying(30),
  "apellido2" Character varying(30),
  "nombre_empresa" Character varying(50),
  "direccion" Character varying(50),
  "ciudad" Character varying(30),
  "cp" Character varying(10),
  "provincia" Character varying(30),
  "pais" Character varying(30)
)
WITH (
  autovacuum_enabled=true)
;

ALTER TABLE "cliente" ADD CONSTRAINT "CLIENTE_PK" PRIMARY KEY ("cif_cliente")
;
-- Table proveedor

CREATE TABLE "proveedor"
(
  "cif_proveedor" Character varying(15) NOT NULL,
  "nombre" Character varying(60),
  "apellido1" Character varying(30),
  "apellido2" Character varying(30),
  "nombre_empresa" Character varying(50),
  "direccion" Character varying(50),
  "ciudad" Character varying(30),
  "cp" Character varying(10),
  "provincia" Character varying(30),
  "pais" Character varying(30)
)
WITH (
  autovacuum_enabled=true)
;

ALTER TABLE "proveedor" ADD CONSTRAINT "PROVEEDOR_PK" PRIMARY KEY ("cif_proveedor")
;
-- Table articulo

CREATE TABLE "articulo"
(
  "cod_articulo" Character varying(5) NOT NULL,
  "cif_proveedor" Character varying(15),
  "nombre_articulo" Character varying(50),
  "caracteristicas" Character varying(100)
)
WITH (
  autovacuum_enabled=true)
;

ALTER TABLE "articulo" ADD CONSTRAINT "ARTICULO_PK" PRIMARY KEY ("cod_articulo")
;
-- Table factura

CREATE TABLE "factura"
(
  "num_factura" Integer NOT NULL,
  "fecha_factura" Date,
  "cif_cliente" Character varying(15)
)
WITH (
  autovacuum_enabled=true)
;

ALTER TABLE "factura" ADD CONSTRAINT "FACTURA_PK" PRIMARY KEY ("num_factura")
;
-- Table detalle_factura

CREATE TABLE "detalle_factura"
(
  "num_factura" Integer NOT NULL,
  "num_detalle_factura" Integer NOT NULL,
  "cod_articulo" Character varying(5),
  "cantidad" Integer,
  "precio" Integer
)
WITH (
  autovacuum_enabled=true)
;

ALTER TABLE "detalle_factura" ADD CONSTRAINT "DETALLE_FACTURA_PK" PRIMARY KEY ("num_factura","num_detalle_factura")
;
-- Create foreign keys (relationships) section ------------------------------------------------- 

ALTER TABLE "articulo" ADD CONSTRAINT "ARTICULO_REL_PROVEEDOR_FK" FOREIGN KEY ("cif_proveedor") REFERENCES "proveedor" ("cif_proveedor") ON DELETE RESTRICT ON UPDATE CASCADE
;

ALTER TABLE "factura" ADD CONSTRAINT "FACTURA_REL_CLIENTE_FK" FOREIGN KEY ("cif_cliente") REFERENCES "cliente" ("cif_cliente") ON DELETE RESTRICT ON UPDATE CASCADE
;

ALTER TABLE "detalle_factura" ADD CONSTRAINT "DETALLE_FACTURA_REL_FACTURA_FK" FOREIGN KEY ("num_factura") REFERENCES "factura" ("num_factura") ON DELETE RESTRICT ON UPDATE CASCADE
;

ALTER TABLE "detalle_factura" ADD CONSTRAINT "DETALLE_FACTURA_REL_ARTICULO_FK" FOREIGN KEY ("cod_articulo") REFERENCES "articulo" ("cod_articulo") ON DELETE RESTRICT ON UPDATE CASCADE
;




