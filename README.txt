TECNOLOGIAS
--------------------------------------------
Se utiliza JAVA 1.8 con frameworks Spring, Hibernate, Primefaces. La base de datos utilizada es PostgreSQL


DESPLIEGUE DE LA APLICACION
--------------------------------------------
Para despliegue de la aplicacion debe ingresarse a la carpeta Lucasian/WEB-INF/classes/config y modificar el archivo conexion.properties con los datos de la base de datos. Posterior reiniciar los servicios para tomar los cambios. Se dispone de enlaces para cada una de las opciones solicitadas. El orden de realizar los registros es seguir el flujo de las opciones de izquierda a derecha


PRUEBAS
--------------------------------------------
Para correr las pruebas se recomienda utilizar una base de datos en blanco sin registros, solo con la estructura de las tablas creadas


CODIGO FUENTE
--------------------------------------------
Para el desarrollo se utilizo Spring Tools 4 for Eclipse. Para la configuracion de la base de datos debe modificarse el archivo conexion.properties ubicado en la carpeta src/main/webapp/config 